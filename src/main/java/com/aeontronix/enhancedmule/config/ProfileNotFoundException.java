/*
 * Copyright (c) Aeontronix 2021
 */

package com.aeontronix.enhancedmule.config;

public class ProfileNotFoundException extends Exception {
    public ProfileNotFoundException(String profile) {
        super("Profile not found: " + profile);
    }
}
